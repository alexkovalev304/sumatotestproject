package com.sumatosoft.userservice;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

import com.sumatosoft.userservice.data.User;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;


public class UserService extends Service {
    public static final int MSG_CLIENT_REGISTER = 1;
    public static final int MSG_CLIENT_UNREGISTER = 2;
    public static final int MSG_GET_USERS = 3;
    public static final int MSG_USER_UPDATE = 4;

    public static final String USERS_KEY = "users";


    public static final int SEND_RESULT_DELAY = 5000;

    private ArrayList<User> users;
    private List<Messenger> clientConnections;

    private Messenger messenger;

    public IBinder onBind(Intent intent) {
        return messenger.getBinder();
    }


    @Override
    public void onCreate() {
        super.onCreate();
        clientConnections = new ArrayList<>();
        messenger = new Messenger(new ServiceIncomingHandler(this));
        hardcodeInitUsers();
    }

    public void addConnection(Messenger messenger) {
        clientConnections.add(messenger);
    }

    public void removeConnection(Messenger messenger) {
        clientConnections.remove(messenger);
    }

    public void sendUsersList(Messenger messenger) {
        if (messenger != null) {
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(USERS_KEY, users);
            try {
                messenger.send(Message.obtain(null, MSG_GET_USERS, 0, 0, bundle));
            } catch (RemoteException e) {
                removeConnection(messenger);
            }
        }
    }

    private void hardcodeInitUsers() {
        users = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            users.add(new User(i, getString(R.string.user_prefix, i)));
        }
    }


    private static class ServiceIncomingHandler extends Handler {
        private WeakReference<UserService> serviceWeakReference;

        ServiceIncomingHandler(UserService service) {
            serviceWeakReference = new WeakReference<>(service);
        }

        @Override
        public void handleMessage(final Message msg) {
            final UserService userService = serviceWeakReference.get();
            if (userService != null) {
                switch (msg.what) {
                    case MSG_CLIENT_REGISTER:
                        userService.addConnection(msg.replyTo);
                        break;
                    case MSG_CLIENT_UNREGISTER:
                        userService.removeConnection(msg.replyTo);
                        break;
                    case MSG_GET_USERS:
                        final Messenger messenger = msg.replyTo;
                        postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                userService.sendUsersList(messenger);
                            }
                        }, SEND_RESULT_DELAY);
                        break;
                }
            }
        }
    }

    public void onUsersChange() {
        for (Messenger client : clientConnections) {
            try {
                client.send(Message.obtain(null, MSG_USER_UPDATE, 0, 0));
            } catch (RemoteException e) {
                clientConnections.remove(client);
            }
        }
    }

}
