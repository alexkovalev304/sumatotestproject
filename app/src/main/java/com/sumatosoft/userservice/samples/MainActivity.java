package com.sumatosoft.userservice.samples;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.sumatosoft.userservice.R;
import com.sumatosoft.userservice.UserServiceHelper;
import com.sumatosoft.userservice.callbacks.UserServiceCallback;
import com.sumatosoft.userservice.data.User;

import java.util.List;

public class MainActivity extends AppCompatActivity implements UserServiceCallback, View.OnClickListener {
    private UserServiceHelper serviceHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        serviceHelper = new UserServiceHelper();

        findViewById(R.id.get_users_btn).setOnClickListener(this);


    }

    public void onStart() {
        super.onStart();
        serviceHelper.bindService(this, this);
    }

    public void onStop() {
        super.onStop();
        serviceHelper.unbindService(this);
    }


    @Override
    public void onConnectionSuccess() {

    }

    @Override
    public void onConnectionError() {

    }


    @Override
    public void onDisconnect() {

    }

    @Override
    public void onUsersChange() {

    }

    @Override
    public void onGetUsers(List<User> users) {
        Toast.makeText(this, "onGetUsers", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.get_users_btn:
                serviceHelper.getUsers();
                break;
        }
    }
}
