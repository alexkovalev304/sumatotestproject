package com.sumatosoft.userservice.callbacks;

import com.sumatosoft.userservice.data.User;

import java.util.List;

/**
 * Created by Alex on 08.04.2017.
 */

public interface UserServiceCallback {

    void onConnectionSuccess();

    void onConnectionError();

    void onDisconnect();

    void onUsersChange();

    void onGetUsers(List<User> users);
}
