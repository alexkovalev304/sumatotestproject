package com.sumatosoft.userservice;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

import com.sumatosoft.userservice.callbacks.UserServiceCallback;
import com.sumatosoft.userservice.data.User;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by Alex on 08.04.2017.
 */

public class UserServiceHelper implements ServiceConnection {
    public static final String USERS_KEY = "users";
    public static final String SERVICE_PACKAGE = "com.sumatosoft.userservice";
    public static final String SERVICE_CLASSNAME = "com.sumatosoft.userservice.UserService";

    private Messenger userService;
    private UserServiceCallback callback;

    private Messenger messenger;
    private boolean isBound;

    public UserServiceHelper() {
        messenger = new Messenger(new HelperIncomingHandler(this));
    }


    public void bindService(Context context, UserServiceCallback callback) {
        this.callback = callback;
        if (context != null) {
            Intent intent = new Intent();
            intent.setComponent(new ComponentName(SERVICE_PACKAGE, SERVICE_CLASSNAME));
            context.bindService(intent, this, Context.BIND_AUTO_CREATE);
            isBound = true;
        }
    }


    public void unbindService(Context context) {
        if (isBound && context != null && userService != null) {
            try {
                Message msg = Message.obtain(null, UserService.MSG_CLIENT_UNREGISTER);
                msg.replyTo = messenger;
                userService.send(msg);
            } catch (RemoteException e) {
                onConnectionError();
            }
            context.unbindService(this);
            isBound = false;
        }
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        userService = new Messenger(service);
        try {
            Message msg = Message.obtain(null, UserService.MSG_CLIENT_REGISTER);
            msg.replyTo = messenger;
            userService.send(msg);
            if (callback != null) {
                callback.onConnectionSuccess();
            }
        } catch (RemoteException e) {
            onConnectionError();
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        if (callback != null) {
            callback.onDisconnect();
        }
        userService = null;
        callback = null;
    }


    private void onConnectionError() {
        if (callback != null) {
            callback.onConnectionError();
        }
    }

    private void onUsersChange() {
        if (callback != null) {
            callback.onUsersChange();
        }
    }

    private void onGetUsers(List<User> users) {
        if (callback != null) {
            callback.onGetUsers(users);
        }
    }

    public void getUsers() {
        if (userService != null) {
            Message msg = Message.obtain(null, UserService.MSG_GET_USERS);
            msg.replyTo = messenger;
            try {
                userService.send(msg);
            } catch (RemoteException e) {
                onConnectionError();
            }
        }
    }


    private static class HelperIncomingHandler extends Handler {
        private WeakReference<UserServiceHelper> helperWeakReference;

        HelperIncomingHandler(UserServiceHelper helper) {
            helperWeakReference = new WeakReference<UserServiceHelper>(helper);
        }

        @Override
        public void handleMessage(Message msg) {
            UserServiceHelper serviceHelper = helperWeakReference.get();
            if (serviceHelper != null) {
                switch (msg.what) {
                    case UserService.MSG_GET_USERS:
                        if (msg.obj != null && msg.obj instanceof Bundle) {
                            Bundle bundle = (Bundle) msg.obj;
                            bundle.setClassLoader(User.class.getClassLoader());
                            List<User> users = bundle.getParcelableArrayList(USERS_KEY);
                            serviceHelper.onGetUsers(users);
                        }
                        break;
                    case UserService.MSG_USER_UPDATE:
                        serviceHelper.onUsersChange();
                        break;
                }
            }
        }
    }
}
