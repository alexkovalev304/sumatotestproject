package com.sumatosoft.userservice.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Alex on 07.04.2017.
 */

public class User implements Parcelable {
    private long id;
    private String name;

    public User(long id, String name) {
        this.id = id;
        this.name = name;
    }

    private User(Parcel in) {
        id = in.readLong();
        name = in.readString();
    }


    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
    }
}
